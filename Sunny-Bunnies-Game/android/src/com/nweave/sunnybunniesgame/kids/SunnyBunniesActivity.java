package com.nweave.sunnybunniesgame.kids;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.nweave.sunnybunniesgame.Kids.InterfaceListener;
import com.nweave.sunnybunniesgame.Kids.SunnyBunnies;

public class SunnyBunniesActivity extends AndroidApplication implements InterfaceListener {
    public View graphicView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sunny_bunnies);
        final AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useImmersiveMode = false;
        final SunnyBunnies app = new SunnyBunnies(this);
        runOnUiThread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void run() {

                graphicView = initializeForView(app, config);
                View view = ((ViewGroup) findViewById(R.id.app));
              //  graphicView.setBackground(getResources().getDrawable(R.drawable.bg));

                graphicView.setBackgroundColor(Color.RED);
                ((ViewGroup) view).addView(graphicView);


            }
        });
    }

    @Override
    public void finishRender() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                graphicView.setBackgroundColor(Color.TRANSPARENT);

            }
        });
    }

}