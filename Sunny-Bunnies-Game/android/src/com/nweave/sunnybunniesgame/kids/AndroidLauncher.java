package com.nweave.sunnybunniesgame.kids;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class AndroidLauncher extends Activity {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {

			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
			setContentView(R.layout.activity_splash);
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					try {

						Intent welcomeNavigationIntent = new Intent(AndroidLauncher.this, SunnyBunniesActivity.class);
						startActivity(welcomeNavigationIntent);
						finish();
					} catch (Exception e) {
//						Log.e(LOG_TAG, "Exception in " + getClass().getEnclosingMethod().toString() + "\n" + e.toString());
//						e.printStackTrace();
					}
				}
			}, 1000);
		} catch (Exception e) {
//			Log.e(LOG_TAG, "Exception in " + getClass().getEnclosingMethod().toString() + "\n" + e.toString());
//			e.printStackTrace();
		}
	}


	}

