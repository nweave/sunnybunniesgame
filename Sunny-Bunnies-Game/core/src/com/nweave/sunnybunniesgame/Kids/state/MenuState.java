package com.nweave.sunnybunniesgame.Kids.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.async.AsyncExecutor;
import com.badlogic.gdx.utils.async.AsyncResult;
import com.badlogic.gdx.utils.async.AsyncTask;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.nweave.sunnybunniesgame.Kids.GifDecoder;
import com.nweave.sunnybunniesgame.Kids.SunnyBunnies;

import java.util.logging.Handler;

public class MenuState extends State {
    private final Texture backgroundMenue;
    private final Preferences pref;
      private final Texture muteBtnTexture;
    private final Texture playBtnTexture;
    private final Texture exitBtnTexture;
    private final Texture unMuteBtnTexture;
    private Image btnMenueImage;
    private Button imageJumpButton;
    private  Texture btnMenue;
    private  Animation<TextureRegion> rainbow;
    private  Animation<TextureRegion> sunnyBunnyOrange;
    private  Animation<TextureRegion> sunnyBunnyPink;
    private  Texture cloud;
    private float elapsed;
    private boolean startPlay=false;
    private AsyncExecutor executor = new AsyncExecutor(4);
    private AsyncResult<Void> task;
    private PlayState p;
    static final float SCREEN_WIDTH = 400; // default screen width
    static final float SCREEN_HEIGHT = 690; // default screen height
    static  Stage stage; // default screen height
    private final FillViewport viewport;
    private final SpriteBatch batch;
    private Music clickSound;
    private boolean isClicked=false;
    private boolean mListner;
    private ChangeListener mChangeListner;
    private Image soundBtn;
    private Texture muteBtn;
    private Image exit;
    private Image unMuteBtn;
    private  boolean scale=false;
    private int n=5;
    private ImageButton playBtn;


    public MenuState(final GameStateManager gsm) {
        super(gsm);
        cam = new OrthographicCamera(SCREEN_WIDTH, SCREEN_HEIGHT);
        viewport = new FillViewport(SCREEN_WIDTH, SCREEN_HEIGHT);
        batch=new SpriteBatch();
        stage = new Stage(viewport, batch);
        Gdx.input.setInputProcessor(stage);
        // preferences
        pref = Gdx.app.getPreferences("preferences");

        clickSound = Gdx.audio.newMusic(Gdx.files.internal("mouse_click.mp3"));

        backgroundMenue = new Texture("background_menue.png");
//        sound = new Texture("music.png");

        muteBtnTexture = new Texture("mute.png");
        unMuteBtnTexture = new Texture("unmute.png");
        playBtnTexture = new Texture("play.png");
        exitBtnTexture = new Texture("exit.png");
//        soundButtons();

//        cloud = new Texture("cloud.png");
        createClickableImage();
        if(pref.getBoolean("mute")){
            unMuteBtn.setVisible(true);
            soundBtn.setVisible(false);
        }
        else {
            unMuteBtn.setVisible(false);
            soundBtn.setVisible(true);


        }
    }


//        createButtonWithImage();

////        rainbow = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("rainbow.gif").read());
//        sunnyBunnyOrange = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("sunnybunnyorange.gif").read());
//        sunnyBunnyPink = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("sunnybunnypink.gif").read());



    private void createButtonWithImage(){
        imageJumpButton = new Button(new TextureRegionDrawable(new TextureRegion(playBtnTexture)));
        imageJumpButton.setSize(120,70);
        imageJumpButton.setPosition(SCREEN_WIDTH/2-imageJumpButton.getWidth(),SCREEN_HEIGHT/2-imageJumpButton.getHeight());
//use ChangeListener instead of InputListener
        stage.addActor(imageJumpButton);

        imageJumpButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!imageJumpButton.isDisabled()) {
                    isClicked = true;
                    imageJumpButton.setDisabled(true);
                    if (!pref.getBoolean("mute", false))
                        clickSound = Gdx.audio.newMusic(Gdx.files.internal("mouse_click.mp3"));
//                sunnyBunnyTrack.setLooping(true);
                    clickSound.play();
                    p = new PlayState(gsm);
                    gsm.set(p);
                    event.handle();
                    imageJumpButton.clearListeners();

                }
            }
        });

    }

    private void  createClickableImage(){
//        btnMenueImage=new Image(playBtnTexture);
        playBtn=new ImageButton(new TextureRegionDrawable(new TextureRegion(playBtnTexture)));
        playBtn.setPosition(SCREEN_WIDTH*0.8f-playBtn.getWidth(),SCREEN_HEIGHT/1.3f-playBtn.getHeight());
        stage.addActor(playBtn);

        soundBtn=new Image(muteBtnTexture);
        soundBtn.setPosition(SCREEN_WIDTH/5-soundBtn.getImageWidth(),SCREEN_HEIGHT/2-soundBtn.getImageWidth());
        stage.addActor(soundBtn);


        unMuteBtn=new Image(unMuteBtnTexture);
        unMuteBtn.setPosition(SCREEN_WIDTH/5-unMuteBtn.getImageWidth(),SCREEN_HEIGHT/2-unMuteBtn.getImageWidth());
        stage.addActor(unMuteBtn);




        exit=new Image(exitBtnTexture);
        exit.setPosition(SCREEN_WIDTH/5-exit.getImageWidth(),SCREEN_HEIGHT/3f-exit.getImageWidth());
        stage.addActor(exit);


        exit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!pref.getBoolean("mute", false)){
                    clickSound.play();
                    clickSound.setVolume(6);}

                Gdx.app.exit();

            }
        });

//        btnMenueImage.addListener(new ClickListener(){
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                if (!pref.getBoolean("mute", false)){
//                    clickSound.play();
//                    clickSound.setVolume(6);}
//                event.handle();
//                p = new PlayState(gsm);
//                gsm.set(p);
////            }
//                dispose();
//            }
//        });
        playBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!pref.getBoolean("mute", false)){
                    clickSound.play();
                    clickSound.setVolume(6);}
                event.handle();
                p = new PlayState(gsm);
                gsm.set(p);
//            }
                dispose();
            }
        });

        soundBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!pref.getBoolean("mute", false)){
                    clickSound.play();
                    clickSound.setVolume(6);}

                pref.putBoolean("mute", true);
                pref.flush();
                    soundBtn.setVisible(false);
                    unMuteBtn.setVisible(true);                }
        });

        unMuteBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                    pref.putBoolean("mute", false);
                    pref.flush();
                    soundBtn.setVisible(true);
                    unMuteBtn.setVisible(false);
//                }

            }
        });
//scale=false;

    }

    private void soundButtons(){
//        soundBtn=new Image(sound);
//        soundBtn.setSize(80,80);
//        soundBtn.setPosition(100,100);
//        stage.addActor(soundBtn);
//
//        muteBtn=new Image(mute);
//        muteBtn.setSize(80,80);
//        muteBtn.setPosition(100,100);
//        stage.addActor(muteBtn);
//
//
//        soundBtn.addListener(new ClickListener(){
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                pref.putBoolean("mute", true);
//                pref.flush();
//                soundBtn.setVisible(false);
//                muteBtn.setVisible(true);
////
////                clickSound.play();
////                clickSound.setVolume(6);
//
//            }
//        });
//
//
//        muteBtn.addListener(new ClickListener(){
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                pref.putBoolean("mute", false);
//                pref.flush();
//                muteBtn.setVisible(false);
//                soundBtn.setVisible(true);
////
////                clickSound.play();
////                clickSound.setVolume(6);
//
//            }
//        });
//
//
//
    }

    @Override
    public void handleInput() {
//            if (Gdx.input.justTouched()) {
//                new Handler()




//                startPlay=true;

//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        // do something important here, asynchronously to the rendering thread
////                        final Result result = createResult();
//                        // post a Runnable to the rendering thread that processes the result
//                        startPlay=true;
//
////                        p = new PlayState(gsm);
//
//                        Gdx.app.postRunnable(new Runnable() {
//                            @Override
//                            public void run() {
//                                gsm.set(new PlayState(gsm));
//
//                                // process the result, e.g. add it to an Array<Result> field of the ApplicationListener.
////                                results.add(result);
////                                gsm.set(p);
////                                startPlay=false;
//
//
//                            }
//                        });
//                    }
//                }).start();
//                float delayInSeconds = 0.5f;
//
//                Timer.instance().scheduleTask(new Timer.Task() {
//                    @Override
//                    public void run() {
//                        //your code here
//                        p = new PlayState(gsm);
//                        gsm.set(p);
//
//                    }
//                }, delayInSeconds);
//                this.render(new SpriteBatch());
//                update(Gdx.graphics.getDeltaTime());

//                startPlay=false;

//            }
    }

    @Override
    public void update(float dt) {
        elapsed+=dt;

        handleInput();

    }

    @Override
    public void render(SpriteBatch sb) {
            // set width and height (add n to current size)

            // re-adjust position
//        scale=false;
//        float elapsed = Gdx.graphics.getDeltaTime();
//        if(task!=null)
//            if(task.isDone()){
//                gsm.set(p);
//
//            }


        sb.begin();

        sb.draw(backgroundMenue,0,0, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
//        sb.draw(btnMenue,(Gdx.graphics.getWidth()/2)-(btnMenue.getWidth()/2),(Gdx.graphics.getHeight()/2)-100,btnMenue.getWidth(),btnMenue.getHeight());
//        sb.draw(rainbow.getKeyFrame(elapsed, true),
//                200,
//                Gdx.graphics.getHeight()/2,
//                    Gdx.graphics.getWidth()/1.5f,
//                Gdx.graphics.getHeight()/2);


//        if(startPlay){
//
//            sb.draw(sunnyBunnyOrange.getKeyFrame(elapsed, true),
//                    Gdx.graphics.getWidth()/2-100,
//                    0,
//                    Gdx.graphics.getWidth()/2,
//                    Gdx.graphics.getHeight()/2.5f);
//        }

//        sb.draw(cloud,Gdx.graphics.getWidth()/1.7f,(Gdx.graphics.getHeight()/1.45f)-cloud.getHeight(),cloud.getWidth()+100,cloud.getHeight());
//        sb.draw(cloud,100,(Gdx.graphics.getHeight()/1.5f)-cloud.getHeight(),cloud.getWidth()+100,cloud.getHeight());
        sb.end();
        stage.draw();



    }

    @Override
    public void dispose() {
        stage.dispose();
        batch.dispose();
        backgroundMenue.dispose();
        clickSound.dispose();

    }


}
