package com.nweave.sunnybunniesgame.Kids.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.nweave.sunnybunniesgame.Kids.Act;
import com.nweave.sunnybunniesgame.Kids.GifDecoder;

import java.beans.Visibility;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JLayeredPane;

import jdk.nashorn.internal.codegen.ClassEmitter;

import static com.badlogic.gdx.Input.Keys.R;


public class PlayState extends State  {
//    private final Box2DDebugRenderer box2DDebugRenderer;
    private final Preferences pref;
    private final Texture happyTube;
    private final Texture desertTube;
    private final Music cheerSound;
    private final Texture background5;
    private final Texture spaceTube;
    private final Music clickSound;
    private final Animation<TextureRegion> explosion;
    private final Texture laddar;
    private  Texture playBtnTexture;
    //                private final Sprite ballSprite;
    private  FillViewport viewport;
    private  World world;
    //    private final Texture topLeaves;
    private final Rectangle[] topTubeLeavesRectangles;
    private  Body birdBody;
    private  Texture background4;
    SpriteBatch batch;
    //	Texture background;
    com.nweave.sunnybunniesgame.Kids.InterfaceListener nativePlatform;
    //	ShapeRenderer shapeRenderer;

    Texture gameover;
    Texture[] birds;
    int flapState = 0;
    float birdY = 0;
    float velocity = 5;
    Circle birdCircle;
    int score = 0;
    int scoringTube = 0;
    BitmapFont font;
    static Stage stage;
    Texture topTube;
    Texture bottomTube;
    float gap = 700;
    float maxTubeOffset;
    Random randomGenerator;
    float tubeVelocity = 4;
    int numberOfTubes = 4;
    float[] tubeX = new float[numberOfTubes];
    float[] tubeOffset = new float[numberOfTubes];
    float distanceBetweenTubes;
    Rectangle[] topTubeRectangles;
    Rectangle[] bottomTubeRectangles;

    int gameState = 0;
    float gravity = (float) 0.5f;
//    private Animation<TextureRegion> animation;
    private float elapsed;
    static AssetManager assetManager;
    private Music sunnyBunnyTrack;
    private Texture background;
//    private Animation<TextureRegion> candyOne;
//    private Animation<TextureRegion> candyTwo;
//    private Rectangle[] candyRect;
//    private Texture apple;
//    private Sprite appleSprite;
//    private Image image;
    //    private ArrayList<Rectangle>candyRectangles =new ArrayList<Rectangle>();
    //               static final float SCREEN_WIDTH = 300; // default screen width
    //                static final float SCREEN_HEIGHT = 690; // default screen height
    static final float PPM = 32; // pixels per meter in Box2D world
    //    private OrthographicCamera cam;
    //    Viewport viewport;
    //    Act appleAct;
    int level=0;
    int totalLevelNum=4;
    int levelScore=10;
    ArrayList<Texture> bg=new ArrayList<Texture>();
    ArrayList<Texture> tubs=new ArrayList<Texture>();
    private Texture background2;
    private Texture levelUp;
    private boolean isGameStateCalled;
//    private Animation<TextureRegion> ballons;
//    private Animation<TextureRegion> bottomFlowers;
    private Texture background1;
    private Texture background3;
//    public static final int WIDTH = 1024;
//    public static final int HEIGHT = 1024;
//    public static final float SCALE = 0.5f;
//    public static final String TITLE = "FlappyBird";
//    private GameStateManager gameStateManager;
    private Rectangle topWall= new Rectangle(0, Gdx.graphics.getHeight(), Gdx.graphics.getWidth(), 5);
    private Music gameOverSnd;

    ShapeRenderer shapeRenderer;
    ShapeRenderer shapeRenderer2;

    //    private Music pointSound;
//    Texture loadingBarBackground;
//    Texture loadingBarProgress;
    static final float SCREEN_WIDTH = 350; // default screen width
    static final float SCREEN_HEIGHT = 600; // default screen height

//    TextureRegion loadingBarProgressStart;
//    TextureRegion loadingBarProgressBody;
//    TextureRegion loadingBarProgressEnd;
//    private ShapeRenderer shapeRenderer;
//    private boolean jump= false;
//    private Act ballAct;
//    private TextButton startMenueButton;
    //                private boolean gameOverIsEntered=false;
    float levelVelocity=-10;
    float levelGravity=0.5f;
    private Image beginToPlay;
    private Image nextBtn;
    private Texture nextBtnTexture;
    private boolean gameOverIsEntered=false;
    public static final String FONT_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=():";
    private BitmapFont cisFont;
    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private  Label scoreLabel;
    private Label scoreLabelValue;
    private Label levelLabel;
    private Image continueBtnImage;
    private boolean levelUpIsEntered=false;
    private boolean levelSoundIsPlay=false;
    private Image nextBtnImage;
    private boolean birdCircleCollide=false;


    protected PlayState(GameStateManager gsm) {
        super(gsm);
        //         box2DDebugRenderer=new Box2DDebugRenderer();
        this.world=super.world;
        background = new Texture("garden01.png");
        //                    apple = new Texture("apple.png");
        background3 = new Texture("desertbackground.png");

        background1 = new Texture("garden02.png");
        background2= new Texture("happybg.png");
        background5= new Texture("space.png");
        background4= new Texture("garden2.jpg");
        //        background3= new Texture("bg-old-one.png");
        bg.add(background);
        bg.add(background3);
        bg.add(background1);
        bg.add(background2);
        bg.add(background5);
        bg.add(background4);


        topTube = new Texture("tree.png");
        desertTube = new Texture("desert_tubs.png");
        bottomTube = new Texture("tree.png");
        laddar = new Texture("laddar.png");
        happyTube = new Texture("happytubs.png");
        spaceTube = new Texture("space_tube.png");
        tubs.add(topTube);
        tubs.add(desertTube);
        tubs.add(laddar);
        tubs.add(happyTube);
        tubs.add(spaceTube);
        tubs.add(bottomTube);



        //                    Gdx.input.setInputProcessor(this);
        gameover = new Texture("game_over.png");
        levelUp = new Texture("LevelUp.png");
//        explosion = new Texture("explosion.png");


        birdCircle = new Circle();
        birdCircle.setRadius(133);
        font = new BitmapFont();
        font.setColor(Color.WHITE);
        font.getData().setScale(10);
        cam = new OrthographicCamera(SCREEN_WIDTH, SCREEN_HEIGHT);
        viewport = new FillViewport(SCREEN_WIDTH, SCREEN_HEIGHT);
        batch=new SpriteBatch();
        stage = new Stage(viewport, batch);
        Gdx.input.setInputProcessor(stage);
        createFont();

        birds = new Texture[2];
        birds[0] = new Texture("new_bird_2.png");
        birds[1] = new Texture("new_bird_2.png");
        nextBtnTexture = new Texture("nextBtn.png");
//        apple = new Texture("apple.png");
        maxTubeOffset = Gdx.graphics.getHeight() / 2 - gap / 2- 100;
        randomGenerator = new Random();
        distanceBetweenTubes = Gdx.graphics.getWidth() * 3 / 4;
        topTubeRectangles = new Rectangle[numberOfTubes];
        topTubeLeavesRectangles = new Rectangle[numberOfTubes];
        bottomTubeRectangles = new Rectangle[numberOfTubes];
//        animation = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("sun.gif").read());
        //            candyOne = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("candy_1.gif").read());
        //            candyTwo = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("candy_2.gif").read());
        //            ballons = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("fireworks2.gif").read());
//        bottomFlowers = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("flowers4.gif").read());
        explosion = GifDecoder.loadGIFAnimation(Animation.PlayMode.NORMAL, Gdx.files.internal("dizzy.gif").read());
//        box2DDebugRenderer = new Box2DDebugRenderer(true, true, true, true, true, true);
        gameOverSnd = Gdx.audio.newMusic(Gdx.files.internal("gameOverSnd.mp3"));
        cheerSound = Gdx.audio.newMusic(Gdx.files.internal("CheerSound.mp3"));

        //        pointSound = Gdx.audio.newMusic(Gdx.files.internal("pointSound.mp3"));
        //        gameOverSnd = Gdx.audio.newMusic(Gdx.files.internal("gameOverSnd.mp3"));
        // preferences
        pref = Gdx.app.getPreferences("preferences");
        clickSound = Gdx.audio.newMusic(Gdx.files.internal("mouse_click.mp3"));

        if (!pref.getBoolean("mute", false)){
            bgSound();}

        addButton();
        startGame();


    }

    private void addButton() {
        //                    BitmapFont mfont = new BitmapFont();
        //                    mfont.setColor(Color.RED);
        //                    mfont.getData().setScale(2);

        //                    TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        //                    textButtonStyle.font = mfont;
        //                    startMenueButton = new TextButton("Touch To Begin", textButtonStyle);
        //                    startMenueButton.setWidth(10);
        //                    startMenueButton.setHeight(10);
        //                    startMenueButton.getLabel().setColor(Color.RED);
        //                    startMenueButton.setPosition(SCREEN_WIDTH/ 2-startMenueButton.getWidth(), SCREEN_HEIGHT/ 2);
        //                    stage.addActor(startMenueButton);
        //
        //                    startMenueButton.addListener(new ClickListener() {
        //                        @Override
        //                        public void clicked(InputEvent event, float x, float y) {
        ////                            super.clicked(event, x, y);
        ////                            gsm.set(new MenuState(gsm));
        //
        //                        }
        //                    });

        //                    Image image=new Image(apple);
        //                    image.setPosition(SCREEN_WIDTH/2,SCREEN_HEIGHT/2);
        //                    image.setSize(30,30);
        //                    stage.addActor(image);
        //                    image.addListener(new ClickListener(){
        //                        @Override
        //                        public void clicked(InputEvent event, float x, float y) {
        ////                            super.clicked(event, x, y);
        //                            gsm.set(new MenuState(gsm));
        //
        //                        }
        //                    });
        Texture btnMenue = new Texture("Touch_to_begin2.png");


        beginToPlay=new Image(btnMenue);
        beginToPlay.setSize(150,150);
        beginToPlay.setPosition(SCREEN_WIDTH/3.5f-beginToPlay.getImageWidth()/2,SCREEN_HEIGHT/3-beginToPlay.getImageWidth()/2);
        stage.addActor(beginToPlay);

        playBtnTexture = new Texture("play.png");
        continueBtnImage=new Image(playBtnTexture);
        continueBtnImage.setSize(120,70);
        continueBtnImage.setPosition(SCREEN_WIDTH/3-continueBtnImage.getImageWidth(),SCREEN_HEIGHT/3f-continueBtnImage.getImageWidth());
        stage.addActor(continueBtnImage);
        continueBtnImage.setVisible(false);


        shapeRenderer = new ShapeRenderer();
        shapeRenderer2 = new ShapeRenderer();
        nextBtnTexture = new Texture("nextBtn.png");
        nextBtnImage=new Image(nextBtnTexture);
        nextBtnImage.setSize(120,70);
        nextBtnImage.setPosition(SCREEN_WIDTH/3-nextBtnImage.getImageWidth(),SCREEN_HEIGHT/3f-nextBtnImage.getImageWidth());
        stage.addActor(nextBtnImage);
        nextBtnImage.setVisible(false);




    }




    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {
        elapsed+=dt;
        world.step(elapsed,6,8);
//        box2DDebugRenderer.render(world, cam.combined);


    }

    @Override
    public void render(SpriteBatch sb) {

        Gdx.gl.glClearColor(0,0,0,1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        createLevels(sb);

        if (gameState == 1) {
            beginToPlay.setVisible(false);
            showGame(sb);
        } else if (gameState == 0){
            if (Gdx.input.justTouched()) {
                gameState = 1;
            }
        } else if (gameState == 2) {
            gameOver(sb);

            //
        }
        else if(gameState == 3){


            levelUp(sb);
        }
        else if(gameState == 4) {
            showGame(sb);

        }
        if (flapState == 0) {
            flapState = 1;
        } else {
            flapState = 0;
        }


        sb.begin();
        font.draw(sb, String.valueOf(score), 100, 200);
        sb.end();

//        birdCircle.set(Gdx.graphics.getWidth() / 2, birdY + birds[flapState].getHeight() / 2, birds[flapState].getWidth() / 2);
        birdCircle.set(Gdx.graphics.getWidth() / 2, birdY + birds[flapState].getHeight() / 2, birdCircle.radius);



        for (int i = 0; i < numberOfTubes; i++) {

//            if (Intersector.overlaps(birdCircle, topTubeRectangles[i]) ||
//                    Intersector.overlaps(birdCircle, bottomTubeRectangles[i])) {
////                sb.begin();
////                sb.draw(explosion, birdCircle.x, birdCircle.y, Gdx.graphics.getWidth()/3, Gdx.graphics.getHeight()/3);
////                sb.end();
////                gravity=10;
//
//
//            }
            if (Intersector.overlaps(birdCircle, topTubeRectangles[i]) ||
                    Intersector.overlaps(birdCircle, bottomTubeRectangles[i])) {
                tubeVelocity=0;
                float delayInSeconds = 1;
                float delayInSecondsGameOver = 3;
                birdCircleCollide=true;
                sb.begin();
//        sb.draw(explosion.getKeyFrame(elapsed, false),birdCircle.x,birdCircle.y,
//                Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/4);

                if(!gameOverIsEntered) {
                    if (!pref.getBoolean("mute", false)){

                        gameOverSnd.play();
                        gameOverSnd.setLooping(false);
                        sunnyBunnyTrack.pause();
                        gameOverIsEntered=true;
                        // sunnyBunnyTrack.dispose();
                    }
                }
                sb.draw(explosion.getKeyFrame(elapsed, false), birdCircle.x,birdCircle.y,
                        birdCircle.radius*2.5f,birdCircle.radius*2);
//                explosion.setFrameDuration(0.7f);
//                velocity = velocity + 2;
//                birdY -= velocity;


                sb.end();

                Timer.instance().scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        //your code here
                        velocity = velocity + 2;
                        birdY -= velocity;


                    }
                }, delayInSeconds);




                Timer.instance().scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        //your code here


                        gameState=2;

                    }
                }, delayInSecondsGameOver);


            }

            if (Intersector.overlaps(birdCircle, topWall)) {
                                            gameState = 2;
                //                            birdY=Gdx.graphics.getHeight()-200;


            }

            //            if (Intersector.overlaps(birdCircle, candyRectangles.get(i))) {
            ////                for(Rectangle rect:candyRectangles){
            //                    Gdx.app.log("test","intersection");
            //                    if(score<(levelScore*2))
            ////                        pointSound.setLooping(false);
            ////                        pointSound.play();
            //
            //                gameState=4;
            //            }
        }
        //        shapeRenderer.end();
//        box2DDebugRenderer.render(world, cam.combined);
        stage.draw();


    }

    @Override
    public void dispose() {
        if(sunnyBunnyTrack!=null){
            sunnyBunnyTrack.dispose();}
        if(gameOverSnd!=null){
            gameOverSnd.dispose();}
        stage.dispose();
        bottomTube.dispose();
        topTube.dispose();
        background.dispose();
        background1.dispose();
        background2.dispose();
        //                    background3.dispose();
        background4.dispose();
        gameover.dispose();
        levelUp.dispose();
        //                    nextBtnTexture.dispose();


    }


    private void createLevels(SpriteBatch sb){
        if(level<=bg.size()-1) {
            sb.begin();
            sb.draw(bg.get(level), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            sb.end();
        }
        else {
            level=0;
            sb.begin();
            sb.draw(bg.get(level), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            sb.end();

        }
//        drawAnimation(sb);

    }





    private void showGame(SpriteBatch sb){
        isGameStateCalled=false;
        calculateScore(sb);
        if(!birdCircleCollide) {
            if (Gdx.input.justTouched()) {
                //                        velocity = -10;

                velocity = levelVelocity;
                gravity = levelGravity;
            }

//            drawEntities(sb);

            if (birdY > 0) {
                velocity = velocity + gravity;
                birdY -= velocity;
            } else {

                gameState = 2;
            }
        }
        drawEntities(sb);

    }


    private  void calculateScore(SpriteBatch sb){

        //score
        if (tubeX[scoringTube] < Gdx.graphics.getWidth() / 2) {
            score++;

            Gdx.app.log("Score ", String.valueOf(score));
            if (scoringTube < numberOfTubes - 1){
                scoringTube++;}

            else{
                scoringTube = 0;}

            if(score>=levelScore*(level+1) &&(!isGameStateCalled)){
                isGameStateCalled=true;
                gameState=3;
                Gdx.app.log("gameState ", "enter_game_state3");
                return;

            }
        }
    }


    private void  drawEntities(SpriteBatch sb){
        sb.begin();

        for (int i = 0; i < numberOfTubes; i++) {
            if (tubeX[i] < -topTube.getWidth()) {
                tubeX[i] += numberOfTubes * distanceBetweenTubes;
                tubeOffset[i] = (randomGenerator.nextFloat() - 0.5f) * (Gdx.graphics.getHeight() - gap - 200);
            } else {
                tubeX[i] = tubeX[i] - tubeVelocity;
            }





            //                batch.draw(apple,tubeX[i], Gdx.graphics.getHeight() / 2 - gap / 2.5f + tubeOffset[i], Gdx.graphics.getWidth()/5f,Gdx.graphics.getHeight()/5f);


            if(birdY<Gdx.graphics.getHeight()-300){
                sb.draw(birds[flapState], Gdx.graphics.getWidth() / 2 - birds[flapState].getWidth() / 2, birdY);

            }else{
                sb.draw(birds[flapState], Gdx.graphics.getWidth() / 2 - birds[flapState].getWidth() / 2, Gdx.graphics.getHeight()-300);

            }
//
//            shapeRenderer.setAutoShapeType(true);
//            shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
//            shapeRenderer.rect(tubeX[i], Gdx.graphics.getHeight() / 2 + gap / 2 + tubeOffset[i], tubs.get(level).getWidth()/100, tubs.get(level).getHeight());
//            shapeRenderer.end();
//
//
//            shapeRenderer2.setAutoShapeType(true);
//            shapeRenderer2.begin(ShapeRenderer.ShapeType.Point);
//            shapeRenderer2.rect(tubeX[i], Gdx.graphics.getHeight() / 2 - gap / 2 - bottomTube.getHeight() + tubeOffset[i],
//                    tubs.get(level).getWidth() ,tubs.get(level).getHeight());
//            shapeRenderer2.end();
            sb.draw(tubs.get(level), tubeX[i], Gdx.graphics.getHeight() / 2 + gap / 2 + tubeOffset[i]);
            sb.draw(tubs.get(level), tubeX[i], Gdx.graphics.getHeight() / 2 - gap / 2 - bottomTube.getHeight() + tubeOffset[i]);



            if(Gdx.graphics.getHeight()/2 + gap / 2 + tubeOffset[i]>=1950){


                topTubeRectangles[i] = new Rectangle(tubeX[i],
                        Gdx.graphics.getHeight()/2 + gap / 2 + tubeOffset[i],
                        topTube.getWidth(), bottomTube.getHeight()*100);
            }
            else {
                //                            topTubeRectangles[i] = new Rectangle(tubeX[i], Gdx.graphics.getHeight() / 2 + gap / 2 + tubeOffset[i],
                //                                    topTube.getWidth(), bottomTube.getHeight());
                topTubeRectangles[i] = new Rectangle(tubeX[i], Gdx.graphics.getHeight() / 2 + gap / 2 + tubeOffset[i],
                        tubs.get(level).getWidth(), tubs.get(level).getHeight());

//                shapeRenderer.begin();
//                shapeRenderer.setAutoShapeType(true);
//                shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
//                shapeRenderer.rect(tubeX[i], Gdx.graphics.getHeight() / 2 + gap / 2 + tubeOffset[i], tubs.get(level).getWidth()/100, tubs.get(level).getHeight());
//                shapeRenderer.end();

            }
//                            topTubeLeavesRectangles[i] = new Rectangle(tubeX[i], Gdx.graphics.getHeight() / 2 + gap /2 + tubeOffset[i],
//                                    topTube.getWidth(),bottomTube.getHeight());
            bottomTubeRectangles[i] = new Rectangle(tubeX[i], Gdx.graphics.getHeight() / 2 - gap / 2 - bottomTube.getHeight() + tubeOffset[i],
                    tubs.get(level).getWidth(), tubs.get(level).getHeight());
            //                candyRectangles.add(i,new Rectangle(tubeX[i],Gdx.graphics.getHeight() / 2 - gap / 2.5f + tubeOffset[i],Gdx.graphics.getWidth()/5f,Gdx.graphics.getHeight()/5f));
//            ShapeRenderer shapeRenderer2;
//            shapeRenderer2 = new ShapeRenderer();

            topWall = new Rectangle(0, Gdx.graphics.getHeight(), Gdx.graphics.getWidth(), 100);
        }
        sb.end();

    }



    private void gameOver(SpriteBatch sb){
        birdCircleCollide=false;


        sb.begin();
        nextBtnImage.setVisible(true);
        velocity = velocity + 5;
        birdY -= velocity;
        if(!gameOverIsEntered) {
            if (!pref.getBoolean("mute", false)){

                gameOverSnd.play();
                gameOverSnd.setLooping(false);
                sunnyBunnyTrack.pause();
                gameOverIsEntered=true;
                // sunnyBunnyTrack.dispose();
            }
        }
        scoreLabelValue.setText(String.valueOf(score));
        scoreLabel.setVisible(true);
        scoreLabelValue.setVisible(true);
        sb.draw(gameover, Gdx.graphics.getWidth() / 2 - gameover.getWidth() / 2,
                Gdx.graphics.getHeight() / 2 - gameover.getHeight() / 2);
        sb.end();

        Gdx.app.log("game over","game over");
        //                    if(!gameOverIsEntered) {
        //                        new Thread(new Runnable() {
        //                            @Override
        //                            public void run() {
        //
        //                                long time = System.currentTimeMillis();
        //                                while (System.currentTimeMillis() < time + 1000) {
        //                                }
        //                                Gdx.app.postRunnable(new Runnable() {
        //                                    @Override
        //                                    public void run() {
        //
        //                                        gameOverIsEntered = true;
        //                                        gsm.set(new MenuState(gsm));
        //
        //                                    }
        //                                });
        //                            }
        //                        }).start();
        //                    }
        //
        //                            if (Gdx.input.justTouched()) {
        nextBtnImage.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //                            super.clicked(event, x, y);
                //                            gsm.set(new MenuState(gsm));
                if (!pref.getBoolean("mute", false)){
                    clickSound.play();
                    clickSound.setVolume(6);}

                gsm.set(new MenuState(gsm));
                //                        gameOverSnd.dispose();
                dispose();
            }
        });




        //                            }
        //                    startMenueButton.addListener(new ChangeListener() {
        //                        @Override
        //                        public void changed (ChangeEvent event, Actor actor) {
        //                            gsm.set(new MenuState(gsm));
        //                            gameOverSnd.dispose();
        //                          }
        //                    });
        //                    startMenueButton.addListener(new ClickListener() {
        //                        @Override
        //                        public void clicked(InputEvent event, float x, float y) {
        ////                            super.clicked(event, x, y);
        ////                            gsm.set(new MenuState(gsm));
        //                        }
        //                    });
    }


    private  void levelUp(SpriteBatch sb){
        sb.begin();

        continueBtnImage.setVisible(true);


        sb.draw(levelUp, Gdx.graphics.getWidth() / 2 - levelUp.getWidth() / 2,
                Gdx.graphics.getHeight() - (levelUp.getHeight()+400));
        //            sb.draw(ballons.getKeyFrame(elapsed, true), Gdx.graphics.getWidth()/3f,Gdx.graphics.getHeight()/2, Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);

        //                    nextBtn.addListener(new ClickListener(){
        //                        @Override
        //                        public void clicked(InputEvent event, float x, float y) {
        //                            Gdx.app.log("state3","state3");
        //                            gameState = 4;
        //                            startGame();
        //
        //                            //                score = 0;
        //                            scoringTube = 0;
        //                            //                velocity *=10 ;
        //                            level+=1;
        //                            levelScore+=1;
        //                            if(levelVelocity>-30){
        //                                levelVelocity-=1;
        //                            }
        //                            if(levelGravity<1.5) {
        //                                levelGravity += 0.1;
        //                            }
        //                            tubeVelocity+=0.2;
        //                        }
        //                    });

        //                float delayInSeconds = 1;
        //
        //                    Timer.instance().scheduleTask(new Timer.Task() {
        //                    @Override
        //                    public void run() {
        //                        //your code here
        //
        //                    if (Gdx.input.justTouched()) {
        if (!pref.getBoolean("mute", false)){

        if(!levelSoundIsPlay) {
            levelSoundIsPlay=true;
            cheerSound.play();
            cheerSound.setLooping(false);
            if(sunnyBunnyTrack!=null)
            sunnyBunnyTrack.stop();
        }
        }
        if(!levelUpIsEntered){
            levelUpIsEntered=true;

            continueBtnImage.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Gdx.app.log("state3","state3");
                    if (!pref.getBoolean("mute", false)){
                        clickSound.play();
                        clickSound.setVolume(6);}

                    cheerSound.stop();
                    if(sunnyBunnyTrack!=null)
                    sunnyBunnyTrack.play();
                    continueBtnImage.setVisible(false);
                    levelSoundIsPlay=false;




                    //                score = 0;
                    scoringTube = 0;
                    //                velocity *=10 ;
                    level+=1;
//                    levelScore+=1;
                    levelLabel.setText("Level : "+(level+1));
                    if(levelVelocity>-30){
                        levelVelocity-=1;
                    }
                    if(levelGravity<1.5) {
                        levelGravity += 0.1;
                    }
                    tubeVelocity+=0.2;
                    gameState = 4;
                    startGame();

                }
            });}
        //                    }
        //                    }, delayInSeconds);
        //                    });
        sb.end();

    }

    private void  createNextImageBtn(){
        nextBtn=new Image(nextBtnTexture);
        nextBtn.setSize(120,70);
        nextBtn.setPosition(SCREEN_WIDTH/3.5f-beginToPlay.getImageWidth()/2,SCREEN_HEIGHT/3-beginToPlay.getImageWidth()/2);
        stage.addActor(nextBtn);

    }



    private void bgSound(){
        sunnyBunnyTrack = Gdx.audio.newMusic(Gdx.files.internal("sunnybunny.mp3"));
        sunnyBunnyTrack.setLooping(true);
        sunnyBunnyTrack.play();

    }

    private void drawAnimation(SpriteBatch sp){
        sp.begin();
//        sp.draw(animation.getKeyFrame(elapsed, true),50, Gdx.graphics.getHeight()-500, Gdx.graphics.getWidth()/4,Gdx.graphics.getHeight()/4);
        //        batch.draw(ballons.getKeyFrame(elapsed, true),0, Gdx.graphics.getHeight()-gap, Gdx.graphics.getWidth()/4,Gdx.graphics.getHeight()/4);
//        sp.draw(bottomFlowers.getKeyFrame(elapsed, true),0,0, Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/4);
//        sp.draw(bottomFlowers.getKeyFrame(elapsed, true),Gdx.graphics.getWidth()/2,0, Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/4);
        sp.end();
    }



    public void startGame() {
        levelLabel.setText("Level : "+(level+1));

        birdY = Gdx.graphics.getHeight() / 2 - birds[0].getHeight() / 2;
        for (int i = 0; i < numberOfTubes; i++) {
            tubeOffset[i] =  (randomGenerator.nextFloat() - 0.5f) * (Gdx.graphics.getHeight() - gap - 200);
            tubeX[i] = Gdx.graphics.getWidth() / 2 - topTube.getWidth() / 2  + Gdx.graphics.getWidth() + i * distanceBetweenTubes;
            topTubeRectangles[i] = new Rectangle();
            bottomTubeRectangles[i] = new Rectangle();
        }

    }


    private void createFont() {
        generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Ciscopic_Regular.otf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 35; // font size
        parameter.characters = FONT_CHARACTERS;
        cisFont = generator.generateFont(parameter);
        Label.LabelStyle label2Style = new Label.LabelStyle();
        label2Style.font = cisFont;
        label2Style.fontColor = Color.valueOf("#ffffff");
        scoreLabel = new Label("Your Score", label2Style);
        scoreLabel.setPosition((SCREEN_WIDTH / 1.8f) - scoreLabel.getWidth() / 2, SCREEN_HEIGHT - (scoreLabel.getHeight() * 1.4f));
        scoreLabel.setVisible(false);

        scoreLabelValue = new Label(score+"", label2Style);
        scoreLabelValue.setPosition((SCREEN_WIDTH/2) - scoreLabel.getWidth()*0.01f , SCREEN_HEIGHT - (scoreLabel.getHeight() *2.5f));
        scoreLabelValue.setVisible(false);

        levelLabel = new Label("Level"+level, label2Style);
        levelLabel.setPosition(stage.getWidth() /1.6f, 20);
        levelLabel.setColor(Color.ORANGE);
        //                    levelLabel.setVisible(false);


        stage.addActor(scoreLabel);
        stage.addActor(scoreLabelValue);
        stage.addActor(levelLabel);
    }




    private void createTopWall(){
        //        topWall= new Rectangle(0, Gdx.graphics.getHeight(), Gdx.graphics.getWidth(), 100);


        BodyDef Box1BodyDef = new BodyDef();
        Box1BodyDef.type = BodyDef.BodyType.StaticBody;
        Box1BodyDef.position.set(0, SCREEN_HEIGHT);

        // The body is also added to the world.
        Body Box1Body = world.createBody(Box1BodyDef);

        // Define the shape.
        PolygonShape Box1Box = new PolygonShape();
        Box1Box.setAsBox(SCREEN_WIDTH, 5);


        Box1Body.createFixture(Box1Box,1.5f);
    }

    private void createBirdBody(){

        BodyDef  bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);


        //ball shape
        CircleShape ballshape = new CircleShape();
        ballshape.setRadius(40);


        // ball fixture defination
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = ballshape ;
        fixtureDef.density = 1000;
        fixtureDef.friction = 0.25f;
        fixtureDef.restitution = 0.25f;

        birdBody = world.createBody(bodyDef);
        birdBody.createFixture(fixtureDef);
        //        birdBody.setGravityScale(0);

    }




}
