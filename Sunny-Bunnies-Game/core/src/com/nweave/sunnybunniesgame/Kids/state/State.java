package com.nweave.sunnybunniesgame.Kids.state;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;

/**
 * Created by Brent on 6/25/2015.
 */
public abstract class State {

    protected GameStateManager gsm;
    protected OrthographicCamera cam;
    protected  World world;
    protected Vector3 mouse;


    protected State(GameStateManager gsm){
        this.gsm = gsm;
        world = new World(new Vector2(0, 0), false);
//        cam = new OrthographicCamera(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);
//        cam.setToOrtho(false, 300, 400);


    }

    public abstract void handleInput();
    public abstract void update(float dt);
    public abstract void render(SpriteBatch sb);
    public abstract void dispose();


}