package com.nweave.sunnybunniesgame.Kids;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.nweave.sunnybunniesgame.Kids.state.GameStateManager;
import com.nweave.sunnybunniesgame.Kids.state.MenuState;

import java.util.ArrayList;
import java.util.Random;


public class SunnyBunnies extends ApplicationAdapter  {
	SpriteBatch batch;
//	Texture background;
    com.nweave.sunnybunniesgame.Kids.InterfaceListener nativePlatform;
//	ShapeRenderer shapeRenderer;

    Texture gameover;
	Texture[] birds;
	int flapState = 0;
	float birdY = 0;
	float velocity = 1;
	Circle birdCircle;
	int score = 0;
	int scoringTube = 0;
	BitmapFont font;
    static Stage stage;
    Texture topTube;
	Texture bottomTube;
	float gap = 900;
    float maxTubeOffset;
    Random randomGenerator;
    float tubeVelocity = 4;
    int numberOfTubes = 4;
    float[] tubeX = new float[numberOfTubes];
    float[] tubeOffset = new float[numberOfTubes];
    float distanceBetweenTubes;
    Rectangle[] topTubeRectangles;
    Rectangle[] bottomTubeRectangles;

	int gameState = 0;
	float gravity = (float) 1.3;
    private Animation<TextureRegion> animation;
    private float elapsed;
    static AssetManager assetManager;
    private Music sunnyBunnyTrack;
    private Texture background;
    private Animation<TextureRegion> candyOne;
    private Animation<TextureRegion> candyTwo;
    private Rectangle[] candyRect;
    private Texture apple;
    private Sprite appleSprite;
    private Image image;
//    private ArrayList<Rectangle>candyRectangles =new ArrayList<Rectangle>();
//   static final float SCREEN_WIDTH = 1440; // default screen width
//    static final float SCREEN_HEIGHT = 1920; // default screen height
    static final float PPM = 32; // pixels per meter in Box2D world
//    private OrthographicCamera cam;
//    Viewport viewport;
//    Act appleAct;
int level=0;
int totalLevelNum=4;
int levelScore=1;
ArrayList<Texture> bg=new ArrayList<Texture>();
    private Texture background2;
    private Texture levelUp;
    private boolean isGameStateCalled;
    private Animation<TextureRegion> ballons;
    private Animation<TextureRegion> bottomFlowers;
    private Texture background1;
    private Texture background3;
    public static final int WIDTH = 1024;
    public static final int HEIGHT = 1024;
    public static final float SCALE = 0.5f;
    public static final String TITLE = "FlappyBird";
    private GameStateManager gameStateManager;

    @Override
	public void create () {
		batch = new SpriteBatch();
        gameStateManager = new GameStateManager();
        Gdx.gl.glClearColor(1, 0, 0, 1);
        gameStateManager.push(new MenuState(gameStateManager));
        nativePlatform.finishRender();
	}

	public SunnyBunnies(InterfaceListener nativePlatform){
        this.nativePlatform = nativePlatform;
    }


	@Override
	public void render () {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gameStateManager.update(Gdx.graphics.getDeltaTime());
        gameStateManager.render(batch);

	}

	@Override
	public void dispose () {
//        sunnyBunnyTrack.dispose();
//        batch.dispose();
	}
}
